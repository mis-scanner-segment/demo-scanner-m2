import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tesseract_ocr/android_ios.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image/image.dart' as img;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:opencv/opencv.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import 'details.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> {
  String _text = '';
  final picker = ImagePicker();

  String? fileName;
  Image? imageNew;
  dynamic res;
  io.File? fileCropper;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Scanner Digit'),
        actions: [
          FlatButton(
              onPressed: scannerText,
              child: const Text(
                'Scan',
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openStorage(context);
        },
        child: const Icon(Icons.add_a_photo),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: imageNew != null
            ? Container(
                child: imageNew,
                width: 400,
                height: 400,
              )
            : Container(
                child: const Center(
                  child: Text("Please select photo"),
                ),
              ),
      ),
    );
  }

  Future scannerText() async {
    showDialog(
        context: context,
        builder: (context) {
          return const CircularProgressIndicator();
        });

    try {
      if (fileCropper != null) {
        print("fileCropper ==> ${fileCropper!.path}");

        String text = await FlutterTesseractOcr.extractText(fileCropper!.path, language: 'ssd');

        // final inputImage = InputImage.fromFilePath(localImage.path);
        // final inputImage = InputImage.fromFilePath(_image!.path);
        // final inputImage = InputImage.fromFile(fileCropper!);
        // final textDetector = GoogleMlKit.vision.textDetector();
        // final RecognisedText recognisedText =
        //     await textDetector.processImage(inputImage);

        _text = "";
        _text = text;
        // for (TextBlock block in recognisedText.blocks) {
        //   for (TextLine line in block.lines) {
        //     _text += line.text + '\n';
        //   }
        // }

        print("result ==> $_text");

        Navigator.of(context).pop();
        setState(() {
          imageNew = null;
        });

        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Details(_text)));
      }
    } catch (e) {
      print(e);
      Navigator.of(context).pop();
    }
  }

  Future<void> openStorage(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: const Text("Select Photo"),
              content: SingleChildScrollView(
                child: ListBody(children: [
                  GestureDetector(
                    child: const Text("Gallery"),
                    onTap: () {
                      Navigator.of(context).pop();
                      _openGallery(context);
                    },
                  ),
                  const Padding(padding: EdgeInsets.all(8)),
                  GestureDetector(
                    child: const Text("Camera"),
                    onTap: () {
                      Navigator.of(context).pop();
                      _openCamera(context);
                    },
                  )
                ]),
              ));
        });
  }

  // helper
  _openGallery(BuildContext context) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    String platformVersion;

    try {
      // res = await ImgProc.threshold(
      //     await io.File(pickedFile!.path).readAsBytes(), 0, 255, ImgProc.adaptiveThreshGaussianC);
     //  res = await ImgProc.adaptiveThreshold(await io.File(pickedFile!.path).readAsBytes(), 127,
     //      ImgProc.adaptiveThreshMeanC, ImgProc.threshBinary, 11, 12);
     //  res = await ImgProc.morphologyEx(res, ImgProc.morphClose, [1, 1]);
     // res = await ImgProc.erode(
     //      await res, [2, 2]);
      // res = await ImgProc.sqrBoxFilter(await res, -2, [2, 2]);
      // res = await ImgProc.erode(
      //     await io.File(pickedFile!.path).readAsBytes(), [2, 3]);
      //res = await ImgProc.applyColorMap(await res, ImgProc.colorMapBone);
      res = await ImgProc.cvtColor( await io.File(pickedFile!.path).readAsBytes(), ImgProc.colorRGB2GRAY);


      res = await ImgProc.threshold(res, 60, 255, ImgProc.threshBinary);
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    fileName =
        "opencv${DateFormat('yyyyMMddHHmmssSSS').format(DateTime.now())}.jpg";
    io.File localImage = await _getPathFileName();

    try {
      if (io.Platform.isAndroid) {
        if (await _requestPermission(Permission.storage)) {
          await localImage.writeAsBytes(
              res.buffer.asUint8List(res.offsetInBytes, res.lengthInBytes));
        }
      } else {
        if (await _requestPermission(Permission.photos)) {
          await localImage.writeAsBytes(
              res.buffer.asUint8List(res.offsetInBytes, res.lengthInBytes));
        }
      }
    } catch (e) {
      print(e);
    }
    fileCropper = await _cropImage(localImage.path);

    setState(() {
      if (pickedFile != null && fileCropper != null) {
        imageNew = Image.file(fileCropper!);
      } else {
        print("No Image selected");
      }
    });
  }

  _openCamera(BuildContext context) async {
    final PickedFile? pickedFile =
        await picker.getImage(source: ImageSource.camera);

    String platformVersion;

    try {
      res = await ImgProc.erode(
          await io.File(pickedFile!.path).readAsBytes(), [2, 2]);
      // res = await ImgProc.sqrBoxFilter(await res, -1, [1, 1]);
      // res = await ImgProc.erode(
      //     await res, [2, 3]);
      res = await ImgProc.applyColorMap(await res, ImgProc.colorMapBone);
      res = await ImgProc.cvtColor(await res, 6);
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    fileName =
        "opencv${DateFormat('yyyyMMddHHmmssSSS').format(DateTime.now())}.jpg";
    io.File localImage = await _getPathFileName();

    try {
      if (io.Platform.isAndroid) {
        if (await _requestPermission(Permission.storage)) {
          await localImage.writeAsBytes(
              res.buffer.asUint8List(res.offsetInBytes, res.lengthInBytes));
        }
      } else {
        if (await _requestPermission(Permission.photos)) {
          await localImage.writeAsBytes(
              res.buffer.asUint8List(res.offsetInBytes, res.lengthInBytes));
        }
      }
    } catch (e) {
      print(e);
    }

    img.Image? original =
        img.decodeImage(io.File(localImage.path).readAsBytesSync());
    img.Image fixed = img.copyRotate(original!, 90);
    io.File(localImage.path).writeAsBytesSync(img.encodeJpg(fixed));

    fileCropper = await _cropImage(localImage.path);

    setState(() {
      if (pickedFile != null && fileCropper != null) {
        imageNew = Image.file(fileCropper!);
      } else {
        print("No Image selected");
      }
    });
  }

  Future<io.File> _getPathFileName() async {
    // final io.Directory appDocDir = await getApplicationDocumentsDirectory();
    final io.Directory appDocDir = await getTemporaryDirectory();
    final String appDocPath = appDocDir.path;
    final io.File localImage =
        await io.File('$appDocPath/assets/images/$fileName')
            .create(recursive: true);

    return localImage;
  }

  Future<bool> _requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  Future<io.File?> _cropImage(String filePath) async {
    io.File localImage = await _getPathFileName();

    io.File? croppedFile = await ImageCropper().cropImage(
        sourcePath: filePath,
        aspectRatioPresets: io.Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        androidUiSettings: const AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: const IOSUiSettings(
          title: 'Cropper',
        ));

    localImage.deleteSync();

    if (croppedFile != null) {
      return croppedFile;
    } else {
      return null;
    }
  }
}
