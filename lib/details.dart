import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Details extends StatefulWidget {
  final String _text;

  Details(this._text);

  @override
  State<StatefulWidget> createState() {
    return _DetailState();
  }
}

class _DetailState extends State<Details> {
  final GlobalKey<ScaffoldState> _key = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text("Scanner"),
        actions: [
          IconButton(
              onPressed: () {
                FlutterClipboard.copy(widget._text).then((value) => _key
                    .currentState
                    ?.showSnackBar(const SnackBar(content: Text("Copied"))));
              },
              icon: const Icon(Icons.copy))
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        alignment: Alignment.center,
        height: double.infinity,
        width: double.infinity,
        child: SelectableText(
            widget._text.isEmpty ? 'No Text Available' : widget._text),
      ),
    );
  }
}
